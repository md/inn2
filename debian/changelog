inn2 (2.7.3~20250201-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.
  * Recommend libdbd-sqlite3-perl for ovsqlite-util.

 -- Marco d'Itri <md@linux.it>  Fri, 21 Feb 2025 18:34:45 +0100

inn2 (2.7.3~20241006-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Mon, 07 Oct 2024 02:12:05 +0200

inn2 (2.7.2-1) unstable; urgency=medium

  * New upstream release.

 -- Marco d'Itri <md@linux.it>  Sun, 23 Jun 2024 16:53:00 +0200

inn2 (2.7.2~20240519-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Fri, 31 May 2024 03:57:34 +0200

inn2 (2.7.2~20240325-3) unstable; urgency=medium

  * Keep improving the autopkgtest.

 -- Marco d'Itri <md@linux.it>  Tue, 02 Apr 2024 01:17:02 +0200

inn2 (2.7.2~20240325-2) unstable; urgency=medium

  * inn2.postinst: hardcode the pathdb default, to support installation
    on the Debian CI infrastructure.

 -- Marco d'Itri <md@linux.it>  Mon, 01 Apr 2024 19:23:25 +0200

inn2 (2.7.2~20240325-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.
  * Try again running the autopkgtest.

 -- Marco d'Itri <md@linux.it>  Mon, 01 Apr 2024 12:29:55 +0200

inn2 (2.7.2~20240212-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.
    + Get the Python link flags from python-config. (Closes: #1061315)

 -- Marco d'Itri <md@linux.it>  Mon, 12 Feb 2024 23:00:26 +0100

inn2 (2.7.2~20240120-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Wed, 24 Jan 2024 23:04:01 +0100

inn2 (2.7.2~20231223-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Mon, 01 Jan 2024 04:18:19 +0100

inn2 (2.7.2~20230917-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Sat, 23 Sep 2023 01:05:36 +0200

inn2 (2.7.2~20230806-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Mon, 07 Aug 2023 02:28:50 +0200

inn2 (2.7.1-1) unstable; urgency=medium

  * New upstream release.
  * Breaks manpages-dev << 6.03-2 to make upgrades smoother, because of
    file(3) and list(3) removed from inn2-dev 2.6.5-1. (Closes: #1035098)

 -- Marco d'Itri <md@linux.it>  Mon, 01 May 2023 19:25:42 +0200

inn2 (2.7.1~20230322-1) unstable; urgency=medium

  * New release candidate 1 of the stable branch.

 -- Marco d'Itri <md@linux.it>  Mon, 27 Mar 2023 04:30:21 +0200

inn2 (2.7.1~20230306-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Thu, 09 Mar 2023 12:18:11 +0100

inn2 (2.7.1~20230220-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Mon, 27 Feb 2023 02:59:01 +0100

inn2 (2.7.1~20230131-1) unstable; urgency=medium

  * New upstream snapshot of the stable branch.

 -- Marco d'Itri <md@linux.it>  Wed, 15 Feb 2023 16:42:07 +0100

inn2 (2.7.1~20230109-3) unstable; urgency=medium

  * Run the autopkgtest with isolation-machine instead of
    isolation-container, because the containers used by the Debian CI
    infrastructure do not have qualified hostnames.

 -- Marco d'Itri <md@linux.it>  Sun, 22 Jan 2023 16:48:56 +0100

inn2 (2.7.1~20230109-2) unstable; urgency=medium

  * Do not start innd when installed by autopkgtest.

 -- Marco d'Itri <md@linux.it>  Sat, 21 Jan 2023 21:00:14 +0100

inn2 (2.7.1~20230109-1) unstable; urgency=medium

  * New upstream snapshot.
  * Add an autopkgtest.

 -- Marco d'Itri <md@linux.it>  Sat, 21 Jan 2023 05:20:26 +0100

inn2 (2.7.1~20221224-1) unstable; urgency=medium

  * New upstream snapshot.
  * Fix the reproducibility of builds for good.

 -- Marco d'Itri <md@linux.it>  Sun, 25 Dec 2022 21:56:36 +0100

inn2 (2.7.1~20221118-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Marco d'Itri <md@linux.it>  Sat, 19 Nov 2022 10:09:34 +0100

inn2 (2.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Enabled support for canlock and ovsqlite.

 -- Marco d'Itri <md@linux.it>  Fri, 18 Nov 2022 00:05:26 +0100

inn2 (2.6.5-1) unstable; urgency=medium

  * New upstream release. Fixes:
    - conflicting man page list(3). (Closes: #1004888)
  * Install a tmpfiles.d file to make sure that /run/news/ exists.
    (Closes: #993211)

 -- Marco d'Itri <md@linux.it>  Mon, 07 Mar 2022 02:22:30 +0100

inn2 (2.6.4-2) unstable; urgency=medium

  * Backported upstream changeset 10348 to fix a failing test on powerpc.

 -- Marco d'Itri <md@linux.it>  Tue, 16 Feb 2021 01:15:55 +0100

inn2 (2.6.4-1) unstable; urgency=medium

  * New upstream release.

 -- Marco d'Itri <md@linux.it>  Thu, 28 Jan 2021 02:05:26 +0100

inn2 (2.6.3+20210104-1) unstable; urgency=medium

  * New upstream snapshot. Fixes:
    + Reproducibility on IPv6-only systems. (Closes: #974024)

 -- Marco d'Itri <md@linux.it>  Sun, 10 Jan 2021 01:22:23 +0100

inn2 (2.6.3+20200601-1) unstable; urgency=medium

  * New upstream snapshot. Contains:
    + A fix for a FTCBFS. (Closes: #943851)
    + New option -b to rnews to save rejected articles. (Closes: #960249)
  * Implemented systemd integration and socket activation.
  * Enabled the Python filters. (Closes: #954710)
  * Changed the remaining references to gpg to gpg1. (Closes: #954414)
  * Versioned the "Pre-Depends: inn2-inews" in inn2, or else inn2 may not
    find the newer libinn on upgrades. (Closes: #947738)

 -- Marco d'Itri <md@linux.it>  Sat, 06 Jun 2020 08:25:31 +0200

inn2 (2.6.3-3) unstable; urgency=medium

  * Backported upstream changeset 10348 to fix upstream changeset 10344.

 -- Marco d'Itri <md@linux.it>  Wed, 18 Sep 2019 11:27:32 +0200

inn2 (2.6.3-2) unstable; urgency=medium

  * Backported upstream changeset 10344 to fix negotiation of DHE
    ciphersuites. (Closes: #931256)

 -- Marco d'Itri <md@linux.it>  Mon, 19 Aug 2019 05:16:59 +0200

inn2 (2.6.3-1) unstable; urgency=medium

  * New upstream release.
  * Depend on cron-daemon to support alternative cron implementations.
    (Closes: #886255)

 -- Marco d'Itri <md@linux.it>  Sun, 17 Feb 2019 17:52:36 +0100

inn2 (2.6.2-1) unstable; urgency=medium

  * New upstream release.

 -- Marco d'Itri <md@linux.it>  Fri, 20 Jul 2018 21:55:50 +0200

inn2 (2.6.1-4) unstable; urgency=medium

  * Force explicit paths for gzip and other programs for the configure
    script to prevent it picking up different paths on old and merged-/usr
    systems. (Closes: #882225)

 -- Marco d'Itri <md@linux.it>  Tue, 21 Nov 2017 17:22:54 +0100

inn2 (2.6.1-3) unstable; urgency=medium

  * Backported a fix to mailpost to remove empty headers. (Closes: #875944)
  * Correctly delete the old radius.conf file. (Closes: #861136)
  * Removed the last vestiges of the inn2-lfs package.

 -- Marco d'Itri <md@linux.it>  Mon, 30 Oct 2017 01:52:26 +0100

inn2 (2.6.1-2) unstable; urgency=medium

  * Change debian/rules to deal with the new dpkg-shlibdeps semantics
    discussed in #854536. (Closes: #855451)

 -- Marco d'Itri <md@linux.it>  Mon, 27 Feb 2017 02:28:32 +0100

inn2 (2.6.1-1) unstable; urgency=medium

  * New upstream snapshot. Fixes:
    + OpenSSL 1.1.0 support. (Closes: #828351)
    + A Perl warning in controlchan. (Closes: #839405)

 -- Marco d'Itri <md@linux.it>  Fri, 30 Dec 2016 01:21:45 +0100

inn2 (2.6.0-2) unstable; urgency=medium

  * Stop rnews from segfaulting while starting. (Closes: #809774)
  * Backported some upstream fixes.

 -- Marco d'Itri <md@linux.it>  Sun, 10 Jan 2016 03:43:00 +0100

inn2 (2.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Stop installing rnews executable by all.
  * Fix inncheck --perm.
  * Use dash instead of bash for the *unbatch scripts.

 -- Marco d'Itri <md@linux.it>  Thu, 31 Dec 2015 00:44:10 +0100

inn2 (2.5.4-3) unstable; urgency=medium

  * Replace the empty /usr/share/doc/inn2-lfs/ of the dummy package with
    the proper symlink. (Closes: #775417)
  * preinst: do not use dpkg --print-architecture. (Closes: #775837)
  * Support selecting the allowed SSL/TLS protocol versions. (Closes: #769046)
  * Disable SSLv2/3 by default.

 -- Marco d'Itri <md@linux.it>  Sun, 15 Feb 2015 20:37:37 +0100

inn2 (2.5.4-2) unstable; urgency=medium

  * preinst: fix the LFS conversion message. (Closes: #750875)
  * postinst: stop innd before rebuilding its databases.
  * Do not make inn2 conflict with inn2-lfs.

 -- Marco d'Itri <md@linux.it>  Mon, 07 Jul 2014 03:19:39 +0200

inn2 (2.5.4-1) unstable; urgency=medium

  * New upstream release. Fixes:
    + "cant select" error messages. (Closes: #708682)
    + incorrectly linking with -L/usr/lib. (Closes: #722785)
  * Do not delete the spool when inn2 replaces inn2-lfs. (Closes: #732047)
  * Stop building the half-broken and unsupportable LFS package and
    implement a transition strategy for its users.
    There is no migration path for timecaf users due to incompatible disk
    formats. A possible solution may involve feeding the articles to the
    new server using the old innxmit binary from a temporary chroot
    containing the old spool, history and /etc/news/. (Closes: #655748)
  * Build a replacement transitional inn2-lfs package on the architectures
    which had the old one.
  * Fix debian/rules to make sure again that wget is used instead of
    simpleftp. (Closes: #729606)
  * Move the example active and newsgroups files out of /usr/share/doc/.
    (Closes: #710417)
  * Use su -s in the maintainer scripts. (Closes: #736818)
  * Stop mentioning trn in the package description. (Closes: #691249)

 -- Marco d'Itri <md@linux.it>  Sun, 01 Jun 2014 20:49:43 +0200

inn2 (2.5.3-3) unstable; urgency=low

  * Fixed the fix for #690128.

 -- Marco d'Itri <md@linux.it>  Mon, 08 Apr 2013 09:21:53 +0200

inn2 (2.5.3-2) unstable; urgency=low

  * Fixed the fix for #652733, which totally broke pgpverify.
    (Closes: #685007)
  * Handle upstream renaming of our conffile /etc/news/motd.news to
    non-conffile /etc/news/motd.nnrpd. If it has not been modified by
    the admin then just remove it. Patch courtesy of Nick Leverton.
    (Closes: #690128)

 -- Marco d'Itri <md@linux.it>  Sun, 07 Apr 2013 21:43:24 +0200

inn2 (2.5.3-1) unstable; urgency=low

  * New upstream release. Fixes:
    + stop sending mail to top1000.org. (Closes: #658438)
    + better check the permissions of the nnrpd TLS private key.
      (Closes: #632068)
  * Do not prefer gpgv2 if it is installed at build time. (Closes: #652733)
  * Added support for dpkg-buildflags and enhanced hardening.

 -- Marco d'Itri <md@linux.it>  Fri, 29 Jun 2012 02:02:31 +0200

inn2 (2.5.2+20110413-1) unstable; urgency=low

  * New upstream snapshot. Fixes:
    + innshellvars overriding $HOME. (Closes: #584234)
  * Switched to libdb5.1. (Closes: #612591)
  * Properly use wget for FTP to fix buildinnkeyring. (Closes: #612677)
  * Fixed the incoming.conf permissions in inncheck.
  * Switched packaging to 3.0 (quilt).

 -- Marco d'Itri <md@linux.it>  Thu, 14 Apr 2011 01:46:20 +0200

inn2 (2.5.2-2) unstable; urgency=low

  * New upstream snapshot, backported many of the new commits up to 9080.
    Fixes:
    + nntpsend documentation. (Closes: #581431)
    + nntpsend silence on locking failure. (Closes: #581558)
    + sendinpaths documentation (Closes: #587410)
  * Enabled SSL support in the standard nnrpd, this time for real.
    (Closes: #581937)
  * Stop modifying the generated configure file. (Closes: #579799)
  * Switched to libdb5.0.

 -- Marco d'Itri <md@linux.it>  Wed, 28 Jul 2010 23:02:06 +0200

inn2 (2.5.2-1) unstable; urgency=low

  * New upstream release. Fixes:
    + perl warning in mailpost. (Closes: #565163)
    + hardcoded maximum number of header fields. (Closes: #566548)
  * Enabled SSL support in the standard nnrpd and replaced the nnrpd-ssl
    binary with a symlink.

 -- Marco d'Itri <md@linux.it>  Mon, 12 Apr 2010 03:27:28 +0200

inn2 (2.5.1-1) unstable; urgency=low

  * New upstream release.
  * In postinst try to stop/start the daemon if ctlinnd xexec fails, to
    support upgrades from versions which had inndstart.

 -- Marco d'Itri <md@linux.it>  Tue, 27 Oct 2009 01:38:21 +0100

inn2 (2.5.0-4) unstable; urgency=medium

  * Added patches changeset_r8556: move dbz.h into the public inn directory.
    (Closes: #533933)
  * Backported more misc fixes.

 -- Marco d'Itri <md@linux.it>  Mon, 10 Aug 2009 14:38:07 +0200

inn2 (2.5.0-3) unstable; urgency=medium

  * Fixed compilation of nnrpd-ssl, which lacked SSL support.
  * Moved INN::Config(3) to the inn2 package.

 -- Marco d'Itri <md@linux.it>  Sat, 11 Jul 2009 00:59:19 +0200

inn2 (2.5.0-2) unstable; urgency=low

  * Various minor fixes courtesy of Julien Élie. (Closes: #533562)
  * Added patch changeset_r8531: include again defines.h and options.h in
    storage.h. (Closes: #533933)
  * Install the includes in /usr/include/inn/ instead of
    /usr/include/inn/inn/ . (Closes: #533947)
  * Added patch changeset_r8521: re-added /etc/news/distributions.
  * Added patch gzip-default-batcher: use gzip instead of compress as the
    default UUCP batcher.
  * Install incoming.conf too with mode 640 since it may contain passwords.
  * Added patch inncheck-permissions: check for the actual owner and group
    used on Debian systems.
  * Set httpdir to /var/www/inn .

 -- Marco d'Itri <md@linux.it>  Sun, 28 Jun 2009 00:59:37 +0200

inn2 (2.5.0-1) unstable; urgency=low

  * New upstream release. Fixes:
    + makehistory(8) and storage.conf(5) man pages.
      (Closes: #511101, #511735)
    + mailpost exit status. (Closes: #517069)
  * Suggest the Kerberos libraries instead of depending on them.
  * Suggest libgd-gd2-noxpm-perl. (Closes: #517655)
  * Removed the logcheck files. (Closes: #517615, #517618)
  * Added patches changeset_r8493: misc backported fixes.
  * Added patch changeset_r8518: fixes HDR/XHDR/XPAT when the overview
    database is inconsistent. (Closes: #533285)

 -- Marco d'Itri <md@linux.it>  Wed, 17 Jun 2009 23:50:43 +0200

inn2 (2.4.5-5) unstable; urgency=medium

  * Added patches u_*: bug fixes from SVN chosen by the upstream maintainer:
    - misc innreport bugs
    - incorrect TLS error handling
    - correctly initialize the status file IP address variables
    - do not send a duplicate reply when TLS negotiation fails
    - correct the permissions checking for XHDR and XPAT
    - do not send a duplicate reply to XOVER/XHDR/XPAT in a empty group
  * Install again our own sasl.conf with the correct paths.
  * Document in README.Debian that STARTTLS and MODE READER do not work
    together. (Closes: #503495)
  * Added patch typo_inn_conf_man fixes a typo in inn.conf(5).
    (Closes: #507256)
  * Updated the md5.c license in debian/copyright.

 -- Marco d'Itri <md@linux.it>  Mon, 15 Dec 2008 00:50:17 +0100

inn2 (2.4.5-4) unstable; urgency=low

  * Backported fixes from SVN: honour the Ad newsfeeds flag and create a
    valid SV for the article body which will correctly match regexps.

 -- Marco d'Itri <md@linux.it>  Wed, 10 Sep 2008 01:36:04 +0200

inn2 (2.4.5-3) unstable; urgency=medium

  * Do not FTBFS with old versions of find. (Closes: #495508)

 -- Marco d'Itri <md@linux.it>  Thu, 28 Aug 2008 04:21:48 +0200

inn2 (2.4.5-2) unstable; urgency=medium

  * Rebuilt with libdb4.6-dev.

 -- Marco d'Itri <md@linux.it>  Sun, 27 Jul 2008 19:23:55 +0200

inn2 (2.4.5-1) unstable; urgency=low

  * New upstream STABLE release.

 -- Marco d'Itri <md@linux.it>  Tue, 01 Jul 2008 01:18:29 +0200

inn2 (2.4.4r-1) unstable; urgency=low

  * New upstream STABLE release. (For real, this time.)
  * On 32 bit architectures, build a new inn2-lfs package with Larges
    Files Support enabled. (Closes: #433751)
  * Enabled support for Kerberos. (Closes: #478775)
  * Rebuilt with perl 5.10. (Closes: #479244)
  * Removed usage of debconf.

 -- Marco d'Itri <md@linux.it>  Sun, 11 May 2008 12:31:56 +0200

inn2 (2.4.4-1) unstable; urgency=low

  * New upstream STABLE snapshot.
    + Rotates innfeed.log. (Closes: #407752)
    + Make inews not fail if MODE READER fails because the connection has
      not been authenticated yet. (Closes: #475059)
  * Removed S from Default-Stop in the init script. (Closes: #471081)
  * Updated debconf translation: pt. (Closes: #444720)
  * Fixed a typo in the name of debian/inn2.logcheck.violations.ignore.
  * Stop overwriting active.times(5) with a symlink, inn now has it.
  * Fixed many minor issues pointed out by Julien Élie. (Closes: #455882)
  * Removed patches merged upstream: daemonize-ovdb_init,
    fix-crash-on-reload, hashfeeds.
  * Remove /var/lib/news/ on purge. (Closes: #455104)

 -- Marco d'Itri <md@linux.it>  Mon, 14 Apr 2008 22:01:48 +0200

inn2 (2.4.3+20070806-1) unstable; urgency=low

  * New upstream STABLE snapshot.
  * Package converted to quilt.
  * Added patch fix-crash-on-reload to fix segfaults when reloading
    incoming.conf (Closes: #361073, #429802, #430190, #430191)
  * Added patch daemonize-ovdb_init to make ovdb_init properly close
    stdin/out/err when it becomes a daemon. (Closes: #433522)
  * Added patch inndstart-sockets-v6only to suppress a startup warning
    about an already opened socket.
  * Fixed the bzip2 path in bunbatch. (Closes: #419429)
  * Removed patches merged upstream: ckpasswd_use_libdb, fix_radius.conf,
    innfeed-fix-getaddrinfo, innfeed-force-ipv4, libdb-4.4.
  * Use --as-needed to not link superfluous libraries.
  * New debconf translations: pt, nl. (Closes: #414921, #415511)
  * Added a logcheck file. (Closes: #405536)

 -- Marco d'Itri <md@linux.it>  Tue, 07 Aug 2007 16:35:06 +0200

inn2 (2.4.3-1) unstable; urgency=low

  * New upstream release. (Closes: #381415)
    + Fixes nnrpd when "localmaxartsize: 0". (Closes: #357370)
  * Removed support for his64v6 and cnfs64, which do not work anyway.
    ****** I am looking for a co-maintainer interested in adding ******
    ****** support to build a inn2-lfs package.                  ******
  * Switched to libdb4.4.
  * New debconf translations: vi, cs, sv. (Closes: #314245, #315211, #339811)
  * Pre-Depends on debconf-2.0 too. (Closes: #331859)
  * Added to innfeed support for a "force-ipv4" configuration option.
    Based on a patch contributed by Henning Makholm. (Closes: #336264)
  * Added to innfeed support for hashed feeds.
  * pgpverify: try harder to find the home directory. (Closes: #307765)
  * Moved nnrpd-ssl to the main package.
  * Added support for libdb to ckpasswd. (Closes: #380644)
  * Use FHS paths in the perl-nocem documentation. (Closes: #365639)
  * Create /var/run/news in the init script if it does not exist.

 -- Marco d'Itri <md@linux.it>  Fri, 18 Aug 2006 11:19:21 +0200

inn2 (2.4.2-3) unstable; urgency=high

  * Fixed upgrades on systems with a non-default pathdb. (Closes: #306765)
  * Added the showtoken program. (Closes: #306837)

 -- Marco d'Itri <md@linux.it>  Sat, 14 May 2005 15:03:56 +0200

inn2 (2.4.2-2) unstable; urgency=medium

  * New upstream snapshot (20050407).
  * Stop providing the inn package. (Closes: #288659)
  * Made postinst continue when makehistory or makedbz fail. (Closes: #292167)
  * Switched to libdb4.3.

 -- Marco d'Itri <md@linux.it>  Fri,  8 Apr 2005 14:51:22 +0200

inn2 (2.4.2-1) unstable; urgency=low

  * New upstream release.
    + Removed patch innreport_nnrpd-ssl.
    + Fixed news2mail, CNFS buffers reporting. (Closes: #282664, #276819)

 -- Marco d'Itri <md@linux.it>  Fri, 24 Dec 2004 17:05:33 +0100

inn2 (2.4.1+20040820-2) unstable; urgency=medium

  * New upstream snapshot (upstream/patches/20040820-to-20040929.diff).
    + make Norbert Tretkowski happy. (Closes: #255324)
    + fix inn2-ssl segfaults on ia64. (Closes: #270875)
  * Conflict with inn and cnews instead of news-transport-system.
    (Closes: #269874)

 -- Marco d'Itri <md@linux.it>  Wed, 29 Sep 2004 17:24:18 +0200

inn2 (2.4.1+20040820-1) unstable; urgency=medium

  * New upstream snapshot.
    + Fixes headers folding in the overview. (Closes: #190207)
    + Fixes headers for articles mailed to moderators. (Closes: #249151)
  * Added a default CA file name to sasl.conf. (Closes: #250201)
  * New patch innreport_nnrpd-ssl: makes innreport correctly parse the
    nnrpd-ssl log entries. (Closes: #250252)
  * New debconf translations: de, ja. (Closes: #263030, #251100)

 -- Marco d'Itri <md@linux.it>  Fri, 20 Aug 2004 19:32:20 +0200

inn2 (2.4.1+20040403-1) unstable; urgency=medium

  * New upstream snapshot. (Closes: #141750)
  * Switched to db4.2. (Closes: #241584)
  * Added catalan debconf template. (Closes: #236668)
  * Removed the patches fix_bindaddress, default-storage.diff and
    fix_reiserfs26.diff because they have been merged upstream.
  * Removed the patch libdb41-fix.diff because it's not needed anymore.

 -- Marco d'Itri <md@linux.it>  Sat,  3 Apr 2004 21:00:31 +0200

inn2 (2.4.1-2) unstable; urgency=medium

  * Fix bindaddress. (Closes: #183812)
  * Fix paths in inn2-ssl. (Closes: #229181)

 -- Marco d'Itri <md@linux.it>  Sat, 24 Jan 2004 17:13:43 +0100

inn2 (2.4.1-1) unstable; urgency=high

  * New upstream release.
    + Fixes buffer overflow, maybe remotely exploitable. (Closes: #226772)
  * Add workaround for 2.6.x reiserfs brokeness. (Closes: #225940)
  * Use pgpverify from -CURRENT to add useless DSA support. (Closes: #222634)
  * Source package converted to DBS.

 -- Marco d'Itri <md@linux.it>  Thu,  8 Jan 2004 20:30:49 +0100

inn2 (2.4.0+20031130-1) unstable; urgency=low

  * New upstream STABLE snapshot. (Closes: #213946)
  * Added russian and spanish debconf messages. (Closes: #219235, #220884)
  * Replaces: inn2-dev to improve upgrades from woody. (Closes: #217219)
  * Added a new his64v6 history method with LFS support. Untested!
    (Closes: #215877)

 -- Marco d'Itri <md@linux.it>  Sun, 30 Nov 2003 22:54:02 +0100

inn2 (2.4.0+20030912-1) unstable; urgency=low

  * New upstream STABLE snapshot.
  * Add again a default storage method to storage.conf. (Closes: #205001)
  * Fix the getlist command line in actsyncd. (Closes: #206283)
  * Added a new cnfs64 method for large cycbufs. The on disk format is not
    compatible with 32-bit cycbufs. The storage tokens are not compatible
    with the tokens of a standard inn package built with --enable-largefiles
    (but they could be converted, let me know if you want to try this).
    This is basically untested and may trash the data you feed it. Please
    let me know if this works for you or not. (Closes: #206828)

 -- Marco d'Itri <md@linux.it>  Fri, 12 Sep 2003 14:07:06 +0200

inn2 (2.4.0+20030808-1) unstable; urgency=medium

  * New upstream snapshot.
  * Fix readers.conf(5) and ckpasswd(8). (Closes: #202098, #202300)
  * Fix innupgrade invocation in postinst. (Closes: #202978)
  * Misc debconf-related fixes courtesy of Christian Perrier
    <bubulle@debian.org>. (Closes: #200517, #200518)
  * Added polish, spanish and french debconf messages.
    (Closes: #202155, #201627)

 -- Marco d'Itri <md@linux.it>  Fri,  8 Aug 2003 13:56:23 +0200

inn2 (2.4.0-3) unstable; urgency=medium

  * Add db_stop to postinst.
  * Fixed inn.conf path in postinst. (Closes: #198578)

 -- Marco d'Itri <md@linux.it>  Wed, 25 Jun 2003 15:15:54 +0200

inn2 (2.4.0-2) unstable; urgency=medium

  * Install all headers in /usr/include/inn. (Closes: #198463, #198464)
  * Added debconf support, patch by <arturcz@hell.pl>.

 -- Marco d'Itri <md@linux.it>  Mon, 23 Jun 2003 19:19:37 +0200

inn2 (2.4.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #182751, #188740, #193967, #194273, #198395)
  * send-uucp.pl is now send-uucp.
  * Switched from db4.0 to db4.1.
  * postinst should not fail if innd cannot start. (Closes: #189966)
  * Depend on perlapi-5.8.0. (Closes: #187717, #192411)
  * Depend on inn2-inews >= 2.3.999+20030227-1. (Closes: #196137)
  * Do not scare admins with wrong postinsg messages. (Closes: #183103)
  * Corrected typo in innupgrade. (Closes: #194444)
  * Added fr.* to /etc/news/moderators. (Closes: #190202)

 -- Marco d'Itri <md@linux.it>  Fri, 20 Jun 2003 18:39:21 +0200

inn2 (2.3.999+20030227-1) unstable; urgency=low

  * New upstream snapshot:
    * Fix expireover segfaults. (Closes: #180462, #179898)
    * Create /var/log/news/path. (Closes: #180168, #180602)
  * Build-Depends on libssl-dev. (Closes: #180662)
  * Fix missing feed name in the log. (Closes: #178842, #181740)
  * Fix news2mail. (Closes: #181086)
  * Fix minor bugs in the init script. (Closes: #180866, #180867)

 -- Marco d'Itri <md@linux.it>  Thu, 27 Feb 2003 19:11:57 +0100

inn2 (2.3.999+20030205-2) unstable; urgency=low

  * New upstream snapshot. (Closes: #179294)
  * Add a new inn2-ssl package. (Closes: #163672)
  * Move wildmat(3) from inn2-dev to inn2. (Closes: #179441)
  * Downgraded to extra priority.
    Most people do not need a local news server, and definitely not INN 2.x.
  * Create /var/{lib,run}/news in postinst.

 -- Marco d'Itri <md@linux.it>  Thu,  6 Feb 2003 15:18:02 +0100

inn2 (2.3.999+20030125-3) unstable; urgency=low

  * Fix rnews breakage. (Closes: #178673)
  * Remove hardcoded paths of egrep, awk, sed, sort, wget. (Closes: #176749)

 -- Marco d'Itri <md@linux.it>  Tue, 28 Jan 2003 01:48:03 +0100

inn2 (2.3.999+20030125-2) unstable; urgency=low

  * Fix broken ctlinnd. (Closes: #178588)

 -- Marco d'Itri <md@linux.it>  Mon, 27 Jan 2003 19:41:03 +0100

inn2 (2.3.999+20030125-1) unstable; urgency=low

  * BEWARE: this is a -CURRENT snapshot. If it breaks you keep both pieces!
    (Closes: #172212, #174938, #176336).
  * Make innreport generate valid HTML. (Closes: #166372)
  * Pre-Depends on inn2-inews. (Closes: #166804)
  * Update gnu.* data in control.ctl. (Closes: #167581)
  * Do not ship rnews suid root! (Closes: #171757)
  * Install /usr/share/doc/inn2/INSTALL.gz (Closes: #174493)

 -- Marco d'Itri <md@linux.it>  Thu, 16 Jan 2003 01:12:53 +0100

inn2 (2.3.3+20020922-5) unstable; urgency=medium

  * Fixed pathtmp (Closes: #162686).
  * Check if the usenet user exists before adding a mail alias
    (Closes: #162731).
  * Fixed a path in sendinpaths (Closes: #163022).

 -- Marco d'Itri <md@linux.it>  Mon,  7 Oct 2002 20:24:16 +0200

inn2 (2.3.3+20020922-4) unstable; urgency=low

  * Applied OVDB fixes, courtesy of Ian Hastie @clara.net (Closes: #162643).

 -- Marco d'Itri <md@linux.it>  Sat, 28 Sep 2002 16:46:57 +0200

inn2 (2.3.3+20020922-3) unstable; urgency=low

  * Fixed absolute path in Makefile (Closes: #162538).

 -- Marco d'Itri <md@linux.it>  Fri, 27 Sep 2002 19:12:10 +0200

inn2 (2.3.3+20020922-1) unstable; urgency=low

  * New STABLE CVS snapshot (Closes: #128725, #137175, #157808, #159105).
  * Made some changes to make INN compile with perl 5.8.0. May be broken.
  * Fix inndf to convert the "infinite" inodes of reiserfs to 2^31 - 1
    (Closes: #124101).
  * Suggests: gnupg instead of pgp.
  * Brand new init script which uses ctlinnd.
  * Removed debian changes to use mkstemp. INN uses a private temp
    directory anyway.
  * Conflicts+Replaces: ninpaths, added the scripts from the inpaths package.
  * Do not depend anymore on libdb3-util, which is only needed by OVDB.
  * Removed signcontrol.
  * Changed control.* and junk groups to status n.
  * Added gpgverify script (Closes: #131412).
  * Added bunbatch script (Closes: #136860).
  * Added /usr/share/doc/inn2/INSTALL.gz (Closes: #156685).
  * Added buildinnkeyring script which downloads PGP keys from ftp.isc.org
    (Closes: #86989).

 -- Marco d'Itri <md@linux.it>  Sun, 22 Sep 2002 21:05:18 +0200
